package main

import (
	"net/http"

	"bitbucket.org/wolfworkslt/fugue-api/internal/handlers"
	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
)

func Routes() http.Handler {
	mux := chi.NewRouter()
	mux.Use(middleware.Logger)
	mux.Use(middleware.NoCache)
	mux.Use(ApiKey)

	// mux.Get("/", handlers.Repo.Home)
	// mux.Get("/health", handlers.Repo.Health)

	mux.Route("/api/v1", func(r chi.Router) {
		// 	// r.Use(randomErrorMiddleware) // Simulate random error, ie. version 1 is buggy.
		mux.Use(apiVersionCtx("v1"))
		r.Get("/", handlers.Repo.Home)
		r.Get("/health", handlers.Repo.Health)
		r.Mount("/memo", MemosV1Router())
	})

	return mux
}

func MemosV1Router() http.Handler {
	r := chi.NewRouter()
	r.Get("/", handlers.Repo.AllMemos)
	r.Get("/latest", handlers.Repo.Latest)
	r.Post("/", handlers.Repo.CreateMemo)

	r.Route("/{id}", func(r chi.Router) {
		r.Get("/", handlers.Repo.GetMemo)
		r.Patch("/", handlers.Repo.UpdateMemo)
		r.Delete("/", handlers.Repo.DeleteMemo)
	})

	return r
}
