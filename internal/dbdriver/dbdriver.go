package dbdriver

import (
	"database/sql"
	"time"

	_ "github.com/go-sql-driver/mysql"
)

//DB holds the database connection pool
type DB struct {
	SQL *sql.DB
}

var dbConn = &DB{}

const maxDbConn = 10
const maxIdleDbConn = 5
const maxDbLifeTime = 5 * time.Minute

//ConnectSql creates database connection pool for database
func ConnectSql(dsn string) (*DB, error) {
	db, err := NewDatabase(dsn)
	if err != nil {
		panic(err)
	}

	db.SetMaxOpenConns(maxDbConn)
	db.SetMaxIdleConns(maxIdleDbConn)
	db.SetConnMaxLifetime(maxDbLifeTime)

	dbConn.SQL = db

	return dbConn, nil
}

//NewDatabase creates a new database for application
func NewDatabase(dsn string) (*sql.DB, error) {
	db, err := sql.Open("mysql", dsn)
	if err != nil {
		return nil, err
	}

	if err = db.Ping(); err != nil {
		return nil, err
	}
	return db, nil
}
