package handlers

import (
	"encoding/json"
	"net/http"
	"strconv"
	"time"

	"bitbucket.org/wolfworkslt/fugue-api/internal/config"
	"bitbucket.org/wolfworkslt/fugue-api/internal/dbdriver"
	"bitbucket.org/wolfworkslt/fugue-api/internal/models"
	"bitbucket.org/wolfworkslt/fugue-api/internal/repository"
	"bitbucket.org/wolfworkslt/fugue-api/internal/repository/dbrepo"
	"github.com/go-chi/chi/v5"
	"github.com/go-playground/validator/v10"
)

var Repo *Repository

type Repository struct {
	App *config.AppConfig
	DB  repository.DatabaseRepo
}

func NewRepo(a *config.AppConfig, db *dbdriver.DB) *Repository {
	return &Repository{
		App: a,
		DB:  dbrepo.NewMysqlRepo(db.SQL, a),
	}
}

func NewHandlers(r *Repository) {
	Repo = r
}

func (m *Repository) Home(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	w.Write([]byte(`{"hello": "world"}`))
}

func (m *Repository) Latest(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json; charset=utf-8")

	memos, err := m.DB.GetMemos(models.Criteria{Limit: 3})
	if err != nil {
		m.App.Logger.Fatalln(err)
	}

	resp, err := json.Marshal(memos)
	if err != nil {
		m.App.Logger.Fatalln(err)
	}
	w.Write([]byte(resp))
}

func (m *Repository) AllMemos(w http.ResponseWriter, r *http.Request) {
	memos, err := m.DB.GetMemos(models.Criteria{Limit: 5, OrderBy: "UpdatedAt", OrderDir: "desc"})
	if err != nil {
		m.App.Logger.Fatalln(err)
	}

	resp, err := json.Marshal(memos)
	if err != nil {
		m.App.Logger.Fatalln(err)
	}
	w.Write([]byte(resp))
}

func (m *Repository) GetMemo(w http.ResponseWriter, r *http.Request) {
	id, _ := strconv.ParseInt(chi.URLParam(r, "id"), 10, 64)
	if id < 1 {
		m.App.Logger.Println("No id")
		w.WriteHeader(http.StatusNotFound)
	} else {

		memo, err := m.DB.GetMemoById(id)

		if memo == nil {
			w.WriteHeader(http.StatusNotFound)
		}

		if err != nil {
			m.App.Logger.Fatalln(err)
			return
		}

		resp, err := json.Marshal(memo)
		if err != nil {
			m.App.Logger.Fatalln(err)
		}

		w.Write([]byte(resp))
		return
	}
}

func (m *Repository) CreateMemo(w http.ResponseWriter, r *http.Request) {

	// maxMemory 32MB
	if err := r.ParseMultipartForm(32 << 20); err != nil {
		m.App.Logger.Printf("ParseForm() err: %v", err)
		return
	}

	resp := models.Response{Success: false}

	model := models.Memo{Title: r.FormValue("title"), Date: time.Now()}

	validate := validator.New()
	err := validate.Struct(model)

	if err != nil {
		resp.StatusCode = http.StatusBadRequest
		resp.Errors = make(map[string]string)
		for _, err := range err.(validator.ValidationErrors) {
			resp.Errors[err.Field()] = err.Tag()
		}
	} else {
		Id, err := m.DB.StoreMemo(model)

		if err != nil {
			resp.StatusCode = http.StatusBadRequest
			m.App.Logger.Println(err.Error())
		}

		if Id > 0 {
			resp.RecordId = Id
			resp.Success = true

			resp.StatusCode = http.StatusCreated
		}
	}

	m.jsonResponse(w, resp)
}

func (m *Repository) UpdateMemo(w http.ResponseWriter, r *http.Request) {
	id, _ := strconv.ParseInt(chi.URLParam(r, "id"), 10, 64)
	if id < 1 {
		m.App.Logger.Println("No id")
		w.WriteHeader(http.StatusBadRequest)
	}

	memo, err := m.DB.GetMemoById(id)
	if err != nil {
		w.WriteHeader(http.StatusNotFound)
	}

	// maxMemory 32MB
	if err := r.ParseMultipartForm(32 << 20); err != nil {
		m.App.Logger.Printf("ParseForm() err: %v", err)
		return
	}

	resp := models.Response{Success: false}
	memo.Title = r.FormValue("title")

	validate := validator.New()
	err = validate.Struct(memo)

	if err != nil {
		resp.StatusCode = http.StatusBadRequest
		resp.Errors = make(map[string]string)
		for _, err := range err.(validator.ValidationErrors) {
			resp.Errors[err.Field()] = err.Tag()
		}
	}

	err = m.DB.UpdateMemo(memo)
	if err != nil {
		resp.StatusCode = http.StatusInternalServerError
		m.App.Logger.Fatalf("Fail to update memo %s", err.Error())
	} else {
		resp.StatusCode = http.StatusAccepted
		resp.Success = true
	}

	m.jsonResponse(w, resp)
}

func (m *Repository) DeleteMemo(w http.ResponseWriter, r *http.Request) {
	id, _ := strconv.ParseInt(chi.URLParam(r, "id"), 10, 64)
	if id < 1 {
		m.App.Logger.Println("No id")
	}

	err := m.DB.DeleteMemo(id)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
	}

	w.WriteHeader(http.StatusOK)
}

func (m *Repository) Health(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	health := models.HealthData{
		Ok: true,
	}

	resp, err := json.Marshal(health)
	if err != nil {
		m.App.Logger.Fatalln(err)
	}
	w.Write([]byte(resp))
}

func (m *Repository) jsonResponse(w http.ResponseWriter, data models.Response) {

	w.WriteHeader(data.StatusCode)
	w.Header().Set("Content-Type", "application/json; charset=utf-8")

	resp, err := json.Marshal(data)
	if err != nil {
		m.App.Logger.Fatalln(err)
	}

	w.Write([]byte(resp))

}
