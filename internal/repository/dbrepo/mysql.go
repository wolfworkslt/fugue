package dbrepo

import (
	"database/sql"
	"strconv"
	"time"

	"bitbucket.org/wolfworkslt/fugue-api/internal/models"
)

func (m *mysqlDbRepo) GetMemos(crit models.Criteria) ([]models.Memo, error) {
	query := "select Id, Title, Date, CreatedAt, UpdatedAt from memos"

	if len(crit.OrderBy) > 0 {
		query = query + " order by " + crit.OrderBy
		if len(crit.OrderDir) > 0 {
			query = query + " " + crit.OrderDir
		}
	}

	if crit.Limit > 0 {
		query = query + " limit " + strconv.Itoa(crit.Limit)
	}

	if crit.Offset > 0 {
		query = query + " offset " + strconv.Itoa(crit.Offset)
	}

	rows, err := m.DB.Query(query)
	var memos []models.Memo

	if err != nil {
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		var memo models.Memo
		err = rows.Scan(&memo.Id, &memo.Title, &memo.Date, &memo.CreatedAt, &memo.UpdatedAt)
		if err != nil {
			return nil, err
		}
		memos = append(memos, memo)
	}

	return memos, nil
}

func (m *mysqlDbRepo) GetMemoById(Id int64) (*models.Memo, error) {
	memo := models.Memo{}
	row := m.DB.QueryRow("select Id, Title, Date, CreatedAt, UpdatedAt from memos where Id = ?", Id)

	err := row.Scan(&memo.Id, &memo.Title, &memo.Date, &memo.CreatedAt, &memo.UpdatedAt)

	if err != nil {
		if err == sql.ErrNoRows {
			m.App.Logger.Println("ble")
			return nil, nil
		} else {
			return nil, err
		}
	}

	return &memo, nil
}

func (m *mysqlDbRepo) StoreMemo(mod models.Memo) (int64, error) {
	result, err := m.DB.Exec("INSERT INTO memos (Title, Date, CreatedAt, UpdatedAt) VALUES (?, ?, ?, ?)", mod.Title, mod.Date.Format("2006-02-01"), time.Now(), time.Now())
	if err != nil {
		return 0, err
	}

	id, err := result.LastInsertId()
	if err != nil {
		return 0, err
	}

	return id, nil
}

func (m *mysqlDbRepo) UpdateMemo(mod *models.Memo) error {
	_, err := m.DB.Exec("Update memos set Title=?, Date=?, UpdatedAt=? where Id=?", mod.Title, mod.Date.Format("2006-02-01"), time.Now(), mod.Id)
	return err
}

func (m *mysqlDbRepo) DeleteMemo(Id int64) error {
	_, err := m.DB.Exec("Delete from memos where Id=?", Id)
	return err
}
