package dbrepo

import (
	"database/sql"

	"bitbucket.org/wolfworkslt/fugue-api/internal/config"
	"bitbucket.org/wolfworkslt/fugue-api/internal/repository"
)

type mysqlDbRepo struct {
	App *config.AppConfig
	DB  *sql.DB
}

func NewMysqlRepo(conn *sql.DB, a *config.AppConfig) repository.DatabaseRepo {
	return &mysqlDbRepo{
		App: a,
		DB:  conn,
	}
}
