package repository

import "bitbucket.org/wolfworkslt/fugue-api/internal/models"

type DatabaseRepo interface {
	GetMemos(crit models.Criteria) ([]models.Memo, error)

	GetMemoById(Id int64) (*models.Memo, error)

	StoreMemo(m models.Memo) (int64, error)

	UpdateMemo(mod *models.Memo) error

	DeleteMemo(Id int64) error
}
