package config

import "log"

type AppConfig struct {
	PortNumber string
	Logger     *log.Logger
}
