package models

import "time"

type HealthData struct {
	Ok bool `json:"ok"`
}

type Criteria struct {
	Limit    int
	Offset   int
	OrderBy  string
	OrderDir string
}

//User is struct to hold user information
type User struct {
	Id        int64
	Username  string
	Name      string
	CreatedAt time.Time
	UpdatedAt time.Time
}

//MemoItem is struct to hold "memo - item" information
type MemoItem struct {
	Id        int64 `json:"id"`
	MemoId    int64
	Position  int    `json:"position"`
	Type      string `json:"type"`
	Content   string `json:"content"`
	CreatedAt time.Time
	UpdatedAt time.Time
}

//Memo is struct to hold "memo" information
type Memo struct {
	Id        int64      `json:"id"`
	Title     string     `json:"title" validate:"required,min=3,max=30"`
	Date      time.Time  `json:"date_time"`
	CreatedAt time.Time  `json:"Created"`
	UpdatedAt time.Time  `json:"Updated"`
	Items     []MemoItem `json:"items"`
}

type PostMemo struct {
	Title string `validate:"required,min=3,max=30"`
}

type Response struct {
	Success    bool              `json:"success"`
	RecordId   int64             `json:"id"`
	Errors     map[string]string `json:"errors"`
	StatusCode int
}
