package main

import (
	"log"
	"net/http"

	"bitbucket.org/wolfworkslt/fugue-api/internal/config"
	"bitbucket.org/wolfworkslt/fugue-api/internal/dbdriver"
	"bitbucket.org/wolfworkslt/fugue-api/internal/handlers"
)

var app config.AppConfig

func main() {

	app.PortNumber = ":8080"
	app.Logger = log.Default()

	//connect to database
	db, err := dbdriver.ConnectSql("root:rootas@/fugueDb?parseTime=true")
	if err != nil {
		app.Logger.Fatal("Cannot connect to database")
	}

	app.Logger.Println("Connected to database")

	repo := handlers.NewRepo(&app, db)
	handlers.NewHandlers(repo)

	srv := http.Server{
		Addr:    app.PortNumber,
		Handler: Routes(),
	}

	app.Logger.Printf("Starting server on: %s", app.PortNumber)

	defer db.SQL.Close()

	err = srv.ListenAndServe()

	app.Logger.Fatal(err)

}
